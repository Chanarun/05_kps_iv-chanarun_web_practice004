import React from 'react'
import {Outlet} from 'react-router-dom'
import {Card} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
export default function OurVision() {
  return (
    <div className='container mb-5'>
      <div className="row">
        <Card className='col-5 me-5 mt-5' >
    <Card.Header className='h-100'>
      <h4 className='m-auto'>Vision</h4>
      <ul>
  <li>To be the best SW Professional Training Center in Cambodia</li>
</ul>  
    </Card.Header>
      </Card>
      
      <Card className='col-5 mt-5' >
    <Card.Header>
      <h4 className='m-auto'>Mission</h4>
      <ul>
  <li>High quuality training and reseach</li>
  <li>Developing Cappacity of SW Experts to be Leaders in IT Field</li>
  <li>Developing sustainable ICT Program</li>
</ul>  
    </Card.Header>
      </Card>

      <Card className='col-5 me-5 mt-5' >
    <Card.Header>
      <h4 className='m-auto'>Strategy</h4>
      <ul>
  <li>Best training method with up to date curriculum and enviroment</li>
  <li>Cooperation with the best IT industry to guarantee student's career and benefits</li>
  <li>Additional Soft Skill, Management. Leadership training</li>
</ul>  
    </Card.Header>
      </Card>

      <Card className='col-5 mt-5 ' >
    <Card.Header className='h-100'>
      <h4 className='m-auto'>Slogan</h4>
      <ul>
  <li>"KSHRD, connects you to varioous opportunites in IT Field."</li>
  <li>Raising brand awareness with continous adverisement of SNS and any other media.</li>
</ul>  
    </Card.Header>
      </Card>
      </div>
        <Outlet />
    </div>
  )
}
