import React from 'react'
import { Card, Button } from 'react-bootstrap'
import phyton from '../image/phyton.jpg';
import udemy from '../image/udemy.jpg';
import 'bootstrap/dist/css/bootstrap.min.css';
export default function Home() {
  return (
    <div className='container '>
      <div className="row">
        <h1>TrendingCourses</h1>
        <Card style={{ width: '18rem', marginRight: '47px', marginBottom: '15px' }}>
          <Card.Img variant="top" src={phyton} />
          <h5 className='m-auto'>Learning Python for Data Analysis and Visualization</h5>
          <p className='m-auto'>Learning python and how to use it to analyze. visualize and present data. Includes tons of sample code and hours of video.</p>
          <h5 className='m-auto text-danger'>12.99$</h5>
          <Button className=' mb-3 m-auto' variant="success" >Buy the course</Button>
        </Card>

        <Card style={{ width: '18rem', marginRight: '47px', marginBottom: '15px' }}>
          <Card.Img variant="top" src={udemy} />
          <h5 className='m-auto'>Learning Python for Data Analysis and Visualization</h5>
          <p className='m-auto'>Learning python and how to use it to analyze. visualize and present data. Includes tons of sample code and hours of video.</p>
          <h5 className='m-auto text-danger'>12.99$</h5>
          <Button className=' mb-3 m-auto' variant="success" >Buy the course</Button>
        </Card>

        <Card style={{ width: '18rem', marginRight: '47px', marginBottom: '15px' }}>
          <Card.Img variant="top" src={udemy} />
          <h5 className='m-auto'>Learning Python for Data Analysis and Visualization</h5>
          <p className='m-auto'>Learning python and how to use it to analyze. visualize and present data. Includes tons of sample code and hours of video.</p>
          <h5 className='m-auto text-danger'>12.99$</h5>
          <Button className=' mb-3 m-auto' variant="success" >Buy the course</Button>
        </Card>

        <Card style={{ width: '18rem', marginBottom: '15px' }}>
          <Card.Img variant="top" src={udemy} />
          <h5 className='m-auto'>Learning Python for Data Analysis and Visualization</h5>
          <p className='m-auto'>Learning python and how to use it to analyze. visualize and present data. Includes tons of sample code and hours of video.</p>
          <h5 className='m-auto text-danger'>12.99$</h5>
          <Button className=' mb-3 m-auto' variant="success" >Buy the course</Button>
        </Card>




      </div>
    </div>
  )
}
