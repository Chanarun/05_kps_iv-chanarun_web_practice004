import React from 'react'
import { Col, Row } from 'react-bootstrap'

export default function Footer() {
  return (
    <div className='container  mt-xxl-5'>
      <Row>
        <Col>
          <div className="box" width={25}>
            <img width={50} src="https://hrd-edu.info/static/media/logo.f368c431.png" alt="" />
            <h6 >© រក្សាសិទ្ធគ្រប់យ៉ាងដោយ KSHRD Center ឆ្នាំ ២០២២</h6>
          </div>
        </Col>

        <Col>
          <div className="box" >
            <h5 className='title '>Adress</h5>
            <p className="address"><b>Address:</b> #12, St 323, Sangkat Boeung Kak II, Khan Toul Kork, Phnom Penh, Cambodia.</p>
          </div></Col>
        <Col>
          <div className="box">
            <h5 className='title'>Contact</h5>
            <p className="tell"> <b>Tel:</b>  012 998 919 (Khmer) </p>
            <p className="tell"> <b>Tel:</b>  085 402 605 (Korean)</p>
            <p className="tell"> <b>Email:</b> info.kshrd@gmail.com phirum.gm@gmail.com</p>
          </div>
        </Col>
      </Row></div>
  )
}
