import React from 'react'
import { Container, Nav, Navbar } from 'react-bootstrap'
import { Link } from 'react-router-dom'


export default function NavMenu() {
  return (
    <>
    <Navbar bg="light">
    <Container>
    <Navbar.Brand href="#home">PT-004</Navbar.Brand>
    <Nav className="me-1">
      <Nav.Link as={Link} to="/">Home</Nav.Link>
      <Nav.Link as={Link} to="/aboutus">About Us</Nav.Link>
      <Nav.Link as={Link} to="/OurVision">Our Vision</Nav.Link>
    </Nav>  
    </Container>
  </Navbar>
  </>
  )
}
